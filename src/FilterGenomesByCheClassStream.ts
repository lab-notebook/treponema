import { Logger, LogLevelDescType } from 'loglevel-colored-prefix';
import { Transform } from 'stream';

class FilterGenomesByCheClassStream extends Transform {
  public processed: number;
  public success: number;
  private readonly cheClass: string;
  private readonly logLevel: LogLevelDescType;
  private readonly log: any;

  public constructor(cheClass: string, logLevel: LogLevelDescType = 'info') {
    super({ objectMode: true });
    this.cheClass = cheClass;
    this.logLevel = logLevel;
    const logger = new Logger(logLevel);
    this.log = logger.getLogger(`FilterGenomesByCheClassStream-${cheClass}`);
    this.processed = 0;
    this.success = 0;
  }

  public _transform(chunk: { value: { stp: { ranks: string[] }; stable_id: string } }, enc: any, next: () => void) {
    this.processed++;
    if (chunk.value.stp.ranks.indexOf(this.cheClass) !== -1) {
      this.success++;
      this.log.info(
        `${this.success}/${this.processed} - ${chunk.value.stable_id} - ${chunk.value.stp.ranks.join(', ')}`,
      );
      this.push(chunk.value);
    }
    next();
  }

  public _flush(next: () => void) {
    this.log.warn(`Processed: ${this.processed}. Success: ${this.success}`);
    next();
  }
}

export { FilterGenomesByCheClassStream };
