import { Logger, LogLevelDescType } from 'loglevel-colored-prefix';
import { Transform } from 'stream';

class FormatFasta extends Transform {
  private readonly logLevel: LogLevelDescType;
  private readonly log: any;

  public constructor(logLevel: LogLevelDescType = 'info') {
    super({ objectMode: true });
    this.logLevel = logLevel;
    const logger = new Logger(logLevel);
    this.log = logger.getLogger(`FormatFasta`);
  }

  public _transform(chunk: { header: string; sequence: string }, enc: any, next: () => void) {
    this.push(`>${chunk.header}\n${chunk.sequence}\n`);
    next();
  }
}

export { FormatFasta };
