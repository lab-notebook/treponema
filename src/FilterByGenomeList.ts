import { Logger, LogLevelDescType } from 'loglevel-colored-prefix';
import { Transform } from 'stream';
import { log } from 'util';

class FilterByGenomeList extends Transform {
  public count: number;
  private readonly genomeList: Set<string>;
  private readonly genomeHitList: Set<string>;
  private readonly logLevel: LogLevelDescType;
  private readonly log: any;

  public constructor(genomeList: string[], logLevel: LogLevelDescType = 'info') {
    super({ objectMode: true });
    this.genomeList = new Set(genomeList);
    this.genomeHitList = new Set();
    this.logLevel = logLevel;
    const logger = new Logger(logLevel);
    this.log = logger.getLogger(`FilterByGenomeList`);
    this.count = 0;
  }

  public _transform(chunk: { value: { stable_id: string; stp: { ranks: any[] } } }, enc: any, next: () => void) {
    try {
      const version = chunk.value.stable_id.split('-')[0];
      const cheClass = chunk.value.stp.ranks[2];
      if (this.genomeList.has(version)) {
        this.count++;
        this.genomeHitList.add(version);
        this.log.info(`Genome: ${version} has a CheA-${cheClass}.`);
        this.log.info(`So far ${this.genomeHitList.size} genomes has at least 1 CheA.`);
        this.push(chunk.value);
      }
      next();
    } catch (err) {
      this.log.error(err);
      this.unpipe();
      throw new Error(err);
    }
  }

  public getStats() {
    return {
      genomesWithChe: this.genomeHitList,
    };
  }
}

export { FilterByGenomeList };
