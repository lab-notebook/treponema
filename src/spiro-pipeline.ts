import fs from "fs";
import { Logger } from "loglevel-colored-prefix";
import Pumpify from "pumpify";
// tslint:disable-next-line: no-submodule-imports
import StreamArray from "stream-json/streamers/StreamArray";
import through2 from "through2";

import { AppendChemosensoryProfile } from "./AppendChemosensoryProfile";
import { AppendGtdbInfo } from "./AppendGtdbInfo";
import { BuildChemoProfile } from "./BuildChemoProfile";
import { PassGenomeInfoViaMiST3Api } from "./PassGenomeInfoViaMiSTApi";

const logger = new Logger("info");
const log = logger.getLogger("spiro-pipeline");

const genomeBufferSize = 100;
const typePosition = 1;
const classPosition = 2;

const sourceFile = "data/production/spiro-generate.mist3.genome.version.json";
const wrGenomeFile = "data/production/wr-pipeline.genome.version.json";
const prefix = "data/production/spiro-pipeline";

const outFileSpiroProfileTable = `${prefix}.profile.table.md`;
const outFileSpiroCheAProfile = `${prefix}.chea.profile.json`;
const outFileReport = `${prefix}.report.json`;

interface IReport {
  mist3Present: number;
  with1CheA: number;
}

const report: IReport = {
  mist3Present: 0,
  with1CheA: 0
};

const cheaStream = fs.createReadStream(sourceFile);
const parser = StreamArray.withParser();

const wrGenomes: string[] = JSON.parse(
  fs.readFileSync(wrGenomeFile).toString()
) as string[];
log.info(
  `Loaded ${wrGenomes.length} genome versions from wr analysis to check against the chea-pipeline dataset.`
);

const adaptorToPassGenomeInfoViaMiSTApi = through2.obj(function(
  chunk: { value: string },
  enc,
  next
) {
  const object = {
    stable_id: `${chunk.value}-locus`
  };
  this.push(object);
  next();
});

let first = true;

const buildProfile = through2.obj(
  function(
    chunk: {
      version: string;
      cheInfo: Array<{ ranks: string[] }>;
      species: string;
      strain: string;
      name: string;
    },
    enc,
    next
  ) {
    if (first) {
      this.push("{\n");
      first = false;
    } else {
      this.push(`,\n`);
    }
    // tslint:disable-next-line: no-unsafe-any
    const info = chunk.cheInfo
      .filter(cheGene => cheGene.ranks[typePosition] === "chea")
      .map(cheGene => cheGene.ranks[classPosition]);

    if (info.length > 0) {
      report.with1CheA++;
    }

    const chewr = wrGenomes.indexOf(chunk.version) !== -1;
    const objectInfo = {
      chea: info,
      chewr,
      species: chunk.species,
      strain: chunk.strain,
      name: chunk.name
    };
    log.info(objectInfo);
    const objStr = JSON.stringify(objectInfo);
    this.push(`"${chunk.version}": ${objStr}`);
    next();
  },
  function(next) {
    this.push("}");
    next();
  }
);

buildProfile.on("end", () => {
  fs.writeFileSync(outFileReport, JSON.stringify(report, null, " "));
});

const spiroPassGenomes = new PassGenomeInfoViaMiST3Api(
  genomeBufferSize,
  "info"
);

spiroPassGenomes.on("end", () => {
  report.mist3Present = spiroPassGenomes.success;
});

const spiroAppendCheInfo = new AppendChemosensoryProfile();
const spiroBuildTable = new BuildChemoProfile();

const spiroWriteTable = fs.createWriteStream(outFileSpiroProfileTable);
const spiroWriteJson = fs.createWriteStream(outFileSpiroCheAProfile);

const appendGtdb = new AppendGtdbInfo();
appendGtdb
  .init()
  .then(() => {
    appendGtdb.on(
      "data",
      (data: { name: string; gtdb: { p: string }; phylum: string }) => {
        try {
          log.info(`Genome ${data.name} is from phylum: ${data.gtdb.p}`);
        } catch (err) {
          log.info(`Genome ${data.name} is from phylum: ${data.phylum}*`);
        }
      }
    );

    const pipeline = new Pumpify.obj(
      parser,
      adaptorToPassGenomeInfoViaMiSTApi,
      spiroPassGenomes,
      appendGtdb,
      spiroAppendCheInfo
    );

    const main = cheaStream.pipe(pipeline);

    main
      .pipe(spiroBuildTable)
      .pipe(spiroWriteTable)
      .on("error", err => {
        log.error(err);
        throw err;
      });

    main
      .pipe(buildProfile)
      .pipe(spiroWriteJson)
      .on("error", err => {
        log.error(err);
        throw err;
      });
  })
  .catch(err => {
    log.error(err);
    throw err;
  });
