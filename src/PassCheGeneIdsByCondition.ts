import { Logger, LogLevelDescType } from 'loglevel-colored-prefix';
import { Genes, Genome } from 'mist3-ts';
import { Transform } from 'stream';

import { ISignalGene } from './interfaces';

class PassCheGeneIdsByCondition extends Transform {
  public processed: number;
  public success: number;
  private readonly logLevel: LogLevelDescType;
  private readonly log: any;
  private readonly cheClass: string | undefined;
  private readonly cheType: string;
  private readonly geneBuffer: number[];
  private readonly bufferSize: number;

  public constructor(
    cheType: string,
    cheClass: string | undefined,
    bufferSize: number = 100,
    logLevel: LogLevelDescType = 'info',
  ) {
    super({ objectMode: true });
    this.logLevel = logLevel;
    const logger = new Logger(logLevel);
    this.log = logger.getLogger(`PassCheGeneIdsByCondition`);
    this.cheClass = cheClass;
    this.cheType = cheType;
    this.bufferSize = bufferSize;
    this.geneBuffer = [];
    this.processed = 0;
    this.success = 0;
  }

  public async _transform(chunk: { version: string; cheInfo: ISignalGene[] }, enc: any, next: () => void) {
    let count = 0
    chunk.cheInfo.forEach((signalGene: ISignalGene) => {
      this.log.debug(
        `Searching ${chunk.version} for ${this.cheType}-${this.cheClass} :: ${signalGene.gene_id} - ${JSON.stringify(signalGene.ranks)}`,
      );
      if (signalGene.ranks[1] === this.cheType) {
        if (signalGene.ranks[2] === this.cheClass) {
          this.log.debug(`${this.cheType}-${this.cheClass} :: Ok!`);
          this.geneBuffer.push(signalGene.gene_id);
          count++
          this.processed++
        }
      }
    });
    this.log.debug(`Genome ${chunk.version} added ${count} to buffer of ${this.cheType}-${this.cheClass}. Total count: ${this.geneBuffer.length}`);
    this.log.debug(this.geneBuffer);
    while (this.geneBuffer.length > this.bufferSize) {
      const genes = this.geneBuffer.splice(0, this.bufferSize).join(',');
      this.log.info(`Asking MiST3 for information about ${this.bufferSize} genes.`);
      const mist3Genes = new Genes('silent');
      const geneInfoList = await mist3Genes.fetchByIds(genes);
      geneInfoList.forEach((geneInfo: any) => {
        geneInfo.cheClass = this.cheClass;
        geneInfo.cheType = this.cheType;
        this.push(geneInfo);
        this.success++
      });
      this.log.info(`Pushing ${geneInfoList.length} genes of ${this.cheType}-${this.cheClass}`);
    }
    next();
  }

  public async _flush(next: () => void) {
    this.log.warn(
      `Flushing information of the last ${this.geneBuffer.length} genes to MiST3 => ${this.cheType}-${this.cheClass}`,
    );
    const mist3Genes = new Genes('silent');
    const geneInfoList = await mist3Genes.fetchByIds(this.geneBuffer.join(','));
    geneInfoList.forEach((geneInfo: any) => {
      geneInfo.cheClass = this.cheClass;
      geneInfo.cheType = this.cheType;
      this.push(geneInfo);
      this.success++
    });
    this.log.info(`Pushed ${geneInfoList.length} genes of ${this.cheType}-${this.cheClass}`);
    this.log.warn(`Processed: ${this.processed}. Success: ${this.success}`);
    next();
  }
}

export { PassCheGeneIdsByCondition };
