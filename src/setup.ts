import fs from 'fs'
import { Logger } from "loglevel-colored-prefix";


const logger = new Logger("info");
const log = logger.getLogger("setup-dir");

const dirList = [
  'data',
  'data/raw',
  'data/production',
  'data/lab',
  'data/processed',
]

dirList.forEach((dir) => {
  if (!fs.existsSync(dir)) {
    log.info(`Creating the directory: ${dir}`)
    fs.mkdirSync(dir)
  }
})

log.info(`Directory structure created.`)
