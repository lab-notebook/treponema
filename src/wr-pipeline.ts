import fs from "fs";
import { Logger } from "loglevel-colored-prefix";
// tslint:disable-next-line: no-submodule-imports
import StreamArray from "stream-json/streamers/StreamArray";
import through2 from "through2";

import { FilterByRegArch } from "./FilterByRegArch";
import { chewrRegArchRule } from "./regArchDefinitions";

const logger = new Logger("info");
const log = logger.getLogger("CheWR-pipeline");

const regArchProcessingLimitPerBatch = 10000;

const sourceFile = "data/raw/chew.noLineBreaks.json";
const prefix = "data/production/wr-pipeline";

const outFileCheWRGenomeVersions = `${prefix}.genome.version.json`;
const outFileReport = `${prefix}.report.json`;

interface IReport {
  wrGenomes: number;
}

const report: IReport = {
  wrGenomes: 0
};

const chewStream = fs.createReadStream(sourceFile);
const streamer = StreamArray.withParser();

streamer.on("error", err => {
  log.error(err);
  throw err;
});

const wrFilter = new FilterByRegArch(
  chewrRegArchRule,
  regArchProcessingLimitPerBatch
);

const uniqGenomes: Set<string> = new Set();
let first = true;
const saveCheWRGenomes = through2.obj(
  function(chunk: { value: { stable_id: string } }, enc, next) {
    const genome = chunk.value.stable_id.split("-")[0];
    if (!uniqGenomes.has(genome)) {
      if (!first) {
        this.push(",\n");
      } else {
        first = false;
      }

      log.info(
        `Saving new genome: ${genome} to save it in list. Total: ${uniqGenomes.size}`
      );
      uniqGenomes.add(genome);
      this.push(`"${genome}"`);
    } else {
      log.info(`Genome ${genome} appears to have more than one chewr.`);
    }
    next();
  },
  function(next) {
    log.info(`There are ${uniqGenomes.size} genomes with CheWR on MiST3.`);
    report.wrGenomes = uniqGenomes.size;
    this.push("\n]");
    next();
  }
);

saveCheWRGenomes.on("end", () => {
  fs.writeFileSync(outFileReport, JSON.stringify(report, null, " "));
});

const writeDown = fs.createWriteStream(outFileCheWRGenomeVersions);
writeDown.write("[\n");

chewStream
  .pipe(streamer)
  .pipe(wrFilter)
  .pipe(saveCheWRGenomes)
  .pipe(writeDown);
