import fs from "fs";
import { Gtdb } from "gtdb-local";
import { Logger } from "loglevel-colored-prefix";
import { Genome } from "mist3-ts";

const logger = new Logger("info");
const log = logger.getLogger("Spirochaetota-pipeline");

const gtdb = new Gtdb();

const prefix = "data/production/spiro-generate";
const outFileVersionList = `${prefix}.genome.version.json`;
const outFileVersionListMist3 = `${prefix}.mist3.genome.version.json`;
const outFileReport = `${prefix}.report.json`;

interface IReport {
  totalGtdbSpiroGenomes: number;
  mist3GtdbSpiroGenomes: number;
}

const report = {
  mist3GtdbSpiroGenomes: 0,
  totalGtdbSpiroGenomes: 0
};

const getAllSpirochaetotas = async () => {
  const db = await gtdb.connectDB();
  const query = {
    selector: {
      p: "Spirochaetota"
    }
  };
  const data: {
    docs: Array<{ _id: string }>;
  } = await db.find(query);
  log.info(
    `There are ${data.docs.length} genomes from the Spirochaetota phylum`
  );
  report.totalGtdbSpiroGenomes = data.docs.length;
  const versionList = data.docs.map((doc: { _id: string }) =>
    doc._id.replace("GB_", "").replace("RS_", "")
  );
  log.info(
    `Sample of version List: ${versionList.slice(0, 3).join(", ")}, ...`
  );
  log.info(`Saving information to ${outFileVersionList}`);
  fs.writeFileSync(outFileVersionList, JSON.stringify(versionList));
  const genomeMist3API = new Genome();
  const genomeMist3Info = (await genomeMist3API.fetchDetailsMany(
    versionList
  )) as Array<{ version: string }>;
  log.info(
    `From ${versionList.length} in GTDB, only ${genomeMist3Info.length} are present in MiST3.`
  );
  report.mist3GtdbSpiroGenomes = genomeMist3Info.length;
  const versionListMist3 = genomeMist3Info.map(gene => gene.version);
  log.info(
    `Saving information of genomes in MiST3 to ${outFileVersionListMist3}`
  );
  fs.writeFileSync(outFileVersionListMist3, JSON.stringify(versionListMist3));
  fs.writeFileSync(outFileReport, JSON.stringify(report, null, " "));
  log.info(`All done here`);
};

getAllSpirochaetotas().catch(err => {
  throw err;
});
