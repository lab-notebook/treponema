interface ISignalGene {
  id: number;
  gene_id: number;
  component_id: number;
  signal_domains_version: number;
  ranks: string[];
  counts: any;
  inputs: string[];
  outputs: string[];
  data: any;
}

interface ICheGene {
  header: string;
  components: number[];
}

interface ITaxonomy {
  p: string;
  o: string;
  c: string;
  f: string;
}

interface IChemoProfile {
  version: string;
  id: string;
  name: string;
  assemblyLevel: string;
  cheProfile: ICheGene[];
  taxonomy: ITaxonomy
}

export { IChemoProfile, ITaxonomy, ICheGene, ISignalGene };
