import { Logger, LogLevelDescType } from 'loglevel-colored-prefix';
import now from 'performance-now';
import { RegArch } from 'regarch';
import { IRegArch } from 'regarch/lib/interfaces';
import { Transform } from 'stream';

const positionStart = 11;
const positionEnds = 12;

class FilterByRegArch extends Transform {
  protected readonly logLevel: LogLevelDescType;
  protected readonly log: any;
  protected counter: number;
  protected rule: IRegArch;
  protected buffer: any[];
  protected bufferSize: number;
  protected numberOfGenesProcessed: number;

  public constructor(rule: IRegArch, bufferSize: number = 1000, logLevel: LogLevelDescType = 'info') {
    super({ objectMode: true });
    this.rule = rule;
    this.logLevel = logLevel;
    const logger = new Logger(logLevel);
    this.log = logger.getLogger(`FilterByRegArch`);
    this.buffer = [];
    this.bufferSize = bufferSize;
    this.numberOfGenesProcessed = 0;
    this.counter = 0;
  }

  public _transform(chunk: any, enc: any, next: () => void) {
    this.numberOfGenesProcessed++;
    if (this.buffer.length < this.bufferSize - 1) {
      this.buffer.push(chunk);
    } else {
      this.buffer.push(chunk);
      this.log.info(
        `Processing ${this.buffer.length} new genes against ${this.rule.patterns[0].name}. Total of ${this.numberOfGenesProcessed}`,
      );
      const paArray = this.buffer.map(gene => ({
        pa: {
          pfam31: this.parsePfam31Info(gene),
        },
      }));
      const s = now();
      const regArch = new RegArch(this.rule);
      const result = regArch.exec(paArray);
      const f = now();
      const time = f - s;
      this.log.info(
        `RegArch processed ${this.buffer.length} genes in ${time.toFixed(2)} milliseconds. Rate: ${(
          this.buffer.length / time
        ).toFixed(2)} genes/millisecond.`,
      );
      this.log.info(`Found ${result.filter(r => r).length} matches in this batch.`);
      this.buffer.forEach((gene, i) => {
        if (result[i]) {
          this.counter++;
          this.push(gene);
        }
      });
      this.buffer = [];
    }
    next();
  }

  public _flush(next: () => void) {
    this.log.debug(`Flushing last ${this.buffer.length} genes from rule: ${this.rule.patterns[0].name}`);
    const paArray = this.buffer.map(gene => ({
      pa: {
        pfam31: this.parsePfam31Info(gene),
      },
    }));
    const regArch = new RegArch(this.rule);
    const s = now()
    const result = regArch.exec(paArray);
    const f = now();
    const time = f - s;
    this.log.info(
      `RegArch processed ${this.buffer.length} genes in ${time.toFixed(2)} milliseconds. Rate: ${(
        this.buffer.length / time
      ).toFixed(2)} genes/millisecond.`,
    );
    this.log.info(`Found ${result.filter(r => r).length} matches in this batch.`);
    this.buffer.forEach((gene, i) => {
      if (result[i]) {
        this.counter++;
        this.push(gene);
      }
    });
    this.log.info(
      `${this.counter}/${this.numberOfGenesProcessed} passed through ${this.rule.patterns[0].name} RegArch filter.`,
    );
    next();
  }

  /**
   * Transform MiST3 pfam31 annotation to RegArch format
   *
   * @protected
   * @memberof FilterByRegArch
   */
  protected mist3ToRegArch = (mist3Pfam: Array<number | string>) => [
    mist3Pfam[0],
    mist3Pfam[positionStart],
    mist3Pfam[positionEnds],
  ];

  /**
   * Transform MiST3 pfam31 annotation to RegArch format
   *
   * @protected
   * @memberof FilterByRegArch
   */
  protected mist3ApiToRegArch = (mist3Pfam: { name: any; env_from: any; env_to: any }) => [
    mist3Pfam.name,
    mist3Pfam.env_from,
    mist3Pfam.env_to,
  ];

  /**
   * Parse pfam31 info from MiST3 into something RegArch reads.
   *
   * @protected
   * @param {({
   *     value: { pfam31: Array<string | number>[] };
   *     aseqInfo: { pfam31: Array<string | number>[] };
   *   })} gene
   * @returns
   * @memberof FilterByRegArch
   */
  protected parsePfam31Info(gene: {
    value: { pfam31: Array<Array<string | number>> };
    aseqInfo: { pfam31: Array<{ name: any; env_from: any; env_to: any }> };
  }) {
    if (gene.value) {
      const value = gene.value.pfam31.map(this.mist3ToRegArch);
      this.log.debug(`pfam31 coded in gene info body.`);
      this.log.debug(value);
      return value;
    } else if (gene.aseqInfo) {
      const value = gene.aseqInfo.pfam31.map(this.mist3ApiToRegArch);
      this.log.debug(`AseqInfo found`);
      this.log.debug(gene.aseqInfo);
      this.log.debug(value);
      return value;
    }
    const errorMesg = `Gene does not have architecture information.`;
    this.log.error(errorMesg);
    this.log.error(gene);
    throw new Error(errorMesg);
  }
}

export { FilterByRegArch };
