import { IRegArch } from "regarch/lib/interfaces";

const chewRegArchRule: IRegArch = {
  patterns: [
    {
      name: "chew-rule",
      npos: [],
      pos: [
        {
          name: "^",
          resource: "ra"
        },
        {
          name: "CheW",
          resource: "pfam31"
        },
        {
          name: "$",
          resource: "ra"
        }
      ]
    }
  ]
};

const chewrRegArchRule: IRegArch = {
  patterns: [
    {
      name: "chewr-rule",
      npos: [],
      pos: [
        {
          name: "^",
          resource: "ra"
        },
        {
          name: "CheW",
          resource: "pfam31"
        },
        {
          name: "CheR",
          resource: "pfam31"
        },
        {
          name: "$",
          resource: "ra"
        }
      ]
    }
  ]
};

const cherRegArchRule: IRegArch = {
  patterns: [
    {
      name: "cher-rule",
      npos: [
        {
          name: "CheR",
          resource: "pfam31"
        }
      ],
      pos: []
    }
  ]
};

export { cherRegArchRule, chewrRegArchRule, chewRegArchRule };
