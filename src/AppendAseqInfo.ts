import { Logger, LogLevelDescType } from 'loglevel-colored-prefix';
import { Genes, Genome, Aseq } from 'mist3-ts';
import { Transform } from 'stream';
import { ISignalGene } from './interfaces';

class AppendAseqInfo extends Transform {
  public processed: number;
  public success: number;
  private readonly logLevel: LogLevelDescType;
  private readonly log: any;
  private aseqGeneBuffer: any[];
  private readonly bufferSize: number;
  private readonly aseqBuffer: string[];

  public constructor(bufferSize: number = 100, logLevel: LogLevelDescType = 'info') {
    super({ objectMode: true });
    this.logLevel = logLevel;
    const logger = new Logger(logLevel);
    this.log = logger.getLogger(`AppendAseqInfo`);
    this.bufferSize = bufferSize;
    this.aseqGeneBuffer = [];
    this.aseqBuffer = [];
    this.processed = 0;
    this.success = 0
  }

  public async _transform(chunk: { aseq_id: any; }, enc: any, next: () => void) {
    this.aseqGeneBuffer.push(chunk);
    this.processed++
    const aseq = chunk.aseq_id;
    if (this.aseqBuffer.indexOf(aseq) === -1) {
      this.log.debug(`${chunk.aseq_id} received`)
      this.aseqBuffer.push(aseq);
    }
    while (this.aseqBuffer.length > this.bufferSize - 1) {
      const aseqListRound = this.aseqBuffer.splice(0, this.bufferSize);
      const mist3Aseq = new Aseq('silent');
      this.log.info(`... waiting on MiST3 to return ${aseqListRound.length} genomes ...`)
      const aseqInfoList = await mist3Aseq.fetchMany(aseqListRound)
      aseqInfoList.forEach((aseqInfo: { id: string }) => {
        const genesMatching = this.aseqGeneBuffer.filter(g => aseqInfo.id === g.aseq_id);
        genesMatching.forEach(g => {
          g.aseqInfo = aseqInfo;
          this.push(g);
          this.success++
        });
      });
      this.aseqGeneBuffer = [];
    }
    next();
  }

  public async _flush(next: () => void) {
    this.log.info(`Getting information of the last ${this.aseqBuffer.length} aseq on MiST3`);
    const mist3Aseq = new Aseq('silent');
    this.log.info(`... waiting on MiST3 to return ${this.aseqBuffer.length} genomes ...`)
    const aseqInfoList = await mist3Aseq.fetchMany(this.aseqBuffer)
    aseqInfoList.forEach((aseqInfo: { id: string }) => {
      const genesMatching = this.aseqGeneBuffer.filter(g => aseqInfo.id === g.aseq_id);
      genesMatching.forEach(g => {
        g.aseqInfo = aseqInfo;
        this.push(g);
        this.success++
      });
    });
    this.log.warn(`Processed: ${this.processed}. Success: ${this.success}`);
    next();
  }
}

export { AppendAseqInfo };
