import { Logger, LogLevelDescType } from "loglevel-colored-prefix";
import { Transform } from "stream";

const positionPfamEnvCoordsStarts = 11;
const positionPfamEnvCoordsEnds = 12;

class GetSeqInfoStream extends Transform {
  public processed: number;
  public success: number;
  private readonly logLevel: LogLevelDescType;
  private readonly log: any;
  private readonly trimDomain: string | undefined;
  private readonly tolerance: number;
  private readonly addTag: string;

  public constructor(
    trimDomain: string | undefined = undefined,
    tolerance: number = 0,
    addTag: string = "",
    logLevel: LogLevelDescType = "info"
  ) {
    super({ objectMode: true });
    this.logLevel = logLevel;
    const logger = new Logger(this.logLevel);
    this.log = logger.getLogger(`GetSeqInfoStream`);
    this.trimDomain = trimDomain;
    this.tolerance = tolerance;
    this.addTag = addTag;
    this.processed = 0;
    this.success = 0;
  }

  public _transform(
    chunk: {
      sequence: string;
      pfam31: any[];
      stable_id: string;
      aseqInfo: { sequence: string; pfam31: any[] };
      cheClass: any;
      cheType: any;
    },
    enc: any,
    next: (err: any) => void
  ) {
    this.processed++;
    try {
      let sequence = "";
      let header = "";
      this.log.debug(chunk);
      if (chunk.sequence && chunk.stable_id) {
        sequence = chunk.sequence;
        header = chunk.stable_id;
      } else {
        sequence = chunk.aseqInfo.sequence;
        header = chunk.stable_id;
      }
      if (chunk.cheType) {
        header += `-${chunk.cheType}`;
      }
      if (chunk.cheClass) {
        header += `-${chunk.cheClass}`;
      }
      if (this.addTag !== "") {
        header += `-${this.addTag}`;
      }
      if (this.trimDomain) {
        let domainCoords = {
          env_from: 0,
          env_to: 0
        };
        if (chunk.pfam31) {
          const cheCoords = chunk.pfam31.find(
            (f: string[] | number[]) => f[0] === this.trimDomain
          );
          if (!cheCoords) {
            this.log.warn(
              `Did not find the domain ${this.trimDomain} in ${header}. Skipping`
            );
            throw new Error("no domain found");
          }
          this.log.info(`still here: ${header}`);
          domainCoords.env_from = cheCoords[positionPfamEnvCoordsStarts];
          domainCoords.env_to = cheCoords[positionPfamEnvCoordsEnds];
        } else {
          domainCoords = chunk.aseqInfo.pfam31.find(
            (f: { name: string }) => f.name === this.trimDomain
          );
          if (!domainCoords) {
            this.log.warn(`Did not find the domain. Skipping`);
            throw new Error("no domain found");
          }
        }
        this.log.debug(`Trimming sequence: ${header}`);
        const start =
          domainCoords.env_from - this.tolerance < 0
            ? 0
            : domainCoords.env_from - this.tolerance;
        const stop: number =
          domainCoords.env_to + this.tolerance > sequence.length
            ? sequence.length
            : domainCoords.env_to + this.tolerance;
        this.log.debug(
          `Envelop coverage of the model for ${this.trimDomain}: ${start} | ${stop}`
        );
        sequence = sequence.substring(start, stop);
      }
      this.log.info(
        `header: ${header}. Sequence: ${sequence.substring(0, 10)}...`
      );
      this.success++;
      this.push({ header, sequence });
      next(null);
    } catch (err) {
      if (err.message === "no domain found") {
        next(null);
      } else {
        this.log.error("Found an error");
        this.log.error(err);
        next(err);
      }
    }
  }
}

export { GetSeqInfoStream };
