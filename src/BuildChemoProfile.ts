import { IGtdbLocalTaxonomyInfo } from 'gtdb-local/lib';
import { Logger, LogLevelDescType } from 'loglevel-colored-prefix';
import { Transform } from 'stream';

import { ICheGene, IChemoProfile, ISignalGene, ITaxonomy } from './interfaces';

const cheClassRankPosition = 2;
const cheTypeRankPosition = 1;

const chemoOrder = ['chea', 'cheb', 'cher', 'ched', 'checx', 'chew', 'chewr', 'chev', 'chez', 'mcp', 'other'];

class BuildChemoProfile extends Transform {
  protected readonly logLevel: LogLevelDescType;
  protected readonly log: any;
  protected headerTypes: string[];
  protected cheProfileBuffer: IChemoProfile[];

  public constructor(logLevel: LogLevelDescType = 'info') {
    super({ objectMode: true });
    this.logLevel = logLevel;
    const logger = new Logger(logLevel);
    this.log = logger.getLogger(`BuildChemoProfile`);
    this.headerTypes = [];
    this.cheProfileBuffer = [];
  }

  public _transform(chunk: any, enc: any, next: () => void) {
    const parsedChunk = this.parse(chunk);
    this.cheProfileBuffer.push(parsedChunk);
    next();
  }

  public _flush(next: () => void) {
    this.sortHeader();
    const HeadLine = `|Genome name|version|Assembly Level|Phylum|Class|Order|Family|${this.headerTypes.join('|')}|\n`;
    this.push(HeadLine);
    const format = `|:-|:-|:-|:-|:-|:-|:-|${this.headerTypes.map(g => ':-:').join('|')}|\n`;
    this.push(format);
    this.cheProfileBuffer.sort((a, b) => (a.name < b.name ? -1 : 1));
    this.cheProfileBuffer.forEach(data => {
      let line = `|${data.name}|${data.version}|${data.assemblyLevel}|${data.taxonomy.p}|${data.taxonomy.c}|${data.taxonomy.o}|${data.taxonomy.f}|`;
      this.headerTypes.forEach(header => {
        const cheType = data.cheProfile.find((item: { header: string }) => item.header === header);
        if (cheType) {
          line += `[${cheType.components.length}](${cheType.components.join(',')})|`;
        } else {
          line += '|';
        }
      });
      line += '\n';
      this.push(line);
    });
    next();
  }

  protected parse(chunk: {
    version: string;
    assembly_level: string;
    id: string;
    name: string;
    cheInfo: ISignalGene[];
    gtdb: IGtdbLocalTaxonomyInfo;
    phylum: string;
    order: string;
    class: string;
    family: string;
  }) {
    const version = chunk.version;
    const id = chunk.id;
    const name = chunk.name;
    const cheInfo = chunk.cheInfo;
    const assemblyLevel = chunk.assembly_level
    const data: IChemoProfile = {
      assemblyLevel,
      cheProfile: [],
      id,
      name,
      taxonomy: {
        c: '',
        f: '',
        o: '',
        p: '',
      },
      version,
    };
    data.cheProfile = this.parseCheInfo(cheInfo);
    data.taxonomy = this.parseTaxonomy(chunk);
    return data;
    // This.log.info(data);
  }

  protected parseCheInfo(cheInfo: ISignalGene[]) {
    const cheProfile: ICheGene[] = [];
    cheInfo
      .filter(cheGene => cheGene.ranks[0] === 'chemotaxis')
      .forEach(cheGene => {
        let type = cheGene.ranks[cheTypeRankPosition];
        if (type === 'chew' && cheGene.counts.hasOwnProperty('CheR')) {
          type = 'chewr';
        }
        const cheClass = cheGene.ranks[cheClassRankPosition];
        const header = `${type}-${cheClass}`;
        if (this.headerTypes.indexOf(header) === -1) {
          this.headerTypes.push(header);
        }
        const item = cheProfile.find(component => component.header === header);
        if (item) {
          item.components.push(cheGene.gene_id);
        } else {
          const newItem = {
            components: [cheGene.gene_id],
            header,
          };
          cheProfile.push(newItem);
        }
      });
    return cheProfile;
  }

  protected parseTaxonomy(chunk: {
    gtdb: IGtdbLocalTaxonomyInfo;
    phylum: string;
    order: string;
    class: string;
    family: string;
  }): ITaxonomy {
    if (chunk.gtdb) {
      return this.parseTaxonomyGtdb(chunk.gtdb);
    }
    return this.parseTaxonomyMiST3(chunk);
  }

  protected parseTaxonomyGtdb(gtdbInfo: { p: string; o: string; c: string; f: string }): ITaxonomy {
    return {
      p: gtdbInfo.p,
      // tslint:disable-next-line: object-literal-sort-keys
      o: gtdbInfo.o,
      c: gtdbInfo.c,
      f: gtdbInfo.f,
    };
  }

  protected parseTaxonomyMiST3(chunk: { phylum: string; order: string; class: string; family: string }): ITaxonomy {
    return {
      p: `${chunk.phylum}*`,
      // tslint:disable-next-line: object-literal-sort-keys
      o: `${chunk.order}*`,
      c: `${chunk.class}*`,
      f: `${chunk.family}*`,
    };
  }

  protected sortHeader() {
    this.headerTypes.sort((a, b) => {
      const [cheTypeA, cheClassA] = a.split('-');
      const [cheTypeB, cheClassB] = b.split('-');
      this.log.debug(`${a}:${b} - First rule: che then mcp`);
      if (cheTypeA[0] < cheTypeB[0]) {
        return -1;
      }
      if (cheTypeA[0] > cheTypeB[0]) {
        return 1;
      }
      this.log.debug(`They are the same`);
      this.log.debug(`${a}:${b} - Second rule: classes`);
      if (cheClassA < cheClassB) {
        return -1;
      }
      if (cheClassB < cheClassA) {
        return 1;
      }
      this.log.debug(`They are the same class`);
      this.log.debug(`${a}:${b} - Third rule: order`);
      if (chemoOrder.indexOf(cheTypeA) < chemoOrder.indexOf(cheTypeB)) {
        return -1;
      }
      if (chemoOrder.indexOf(cheTypeB) < chemoOrder.indexOf(cheTypeA)) {
        return 1;
      }
      this.log.debug(`${a}:${b} - No rule`);
      return 0;
    });
  }
}

export { BuildChemoProfile };
