import fs from 'fs';
import { Logger } from 'loglevel-colored-prefix';
import StreamArray from 'stream-json/streamers/StreamArray';
import through2 from 'through2';

import { FormatFasta } from './FormatFasta';
import { GetSeqInfoStream } from './GetSeqInfoStream';

const logger = new Logger('info');
const log = logger.getLogger('MakeCheAFasta');

const sourceFile = 'data/raw/chea.noLineBreaks.json';
const cheaFasta = `data/production/chea.all.CheW.fa`

const cheaStream = fs.createReadStream(sourceFile);
const streamer = StreamArray.withParser();

streamer.on('error', err => {
  log.error(err);
  throw err;
});

let counter = 0;

const count = through2.obj(function(chunk, enc, next) {
  counter++;
  if (counter % 1000 === 0) {
    log.info(`Processing ${counter}`);
    log.info(`Sample:`);
    log.info(`header: ${chunk.value.header}`);
    log.info(`sequence: ${chunk.value.sequence}`);
  }
  this.push(chunk.value);
  next();
});

count.on('end', () => {
  log.info(`All processed: ${counter} sequences.`);
});

const getSeq = new GetSeqInfoStream('CheW', 0, 'chea', 'warn');

const formatFasta = new FormatFasta()
const writeDown = fs.createWriteStream(cheaFasta)

cheaStream
  .pipe(streamer)
  .pipe(count)
  .pipe(getSeq)
  .pipe(formatFasta)
  .pipe(writeDown);

