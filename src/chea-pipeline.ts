import fs from "fs";
import { Logger } from "loglevel-colored-prefix";
import Pumpify from "pumpify";
// tslint:disable-next-line: no-submodule-imports
import StreamArray from "stream-json/streamers/StreamArray";
import through2 from "through2";

import { AppendAseqInfo } from "./AppendAseqInfo";
import { AppendChemosensoryProfile } from "./AppendChemosensoryProfile";
import { AppendGtdbInfo } from "./AppendGtdbInfo";
import { BuildChemoProfile } from "./BuildChemoProfile";
import { FilterByRegArch } from "./FilterByRegArch";
import { FilterGenomesByCheClassStream } from "./FilterGenomesByCheClassStream";
import { FormatFasta } from "./FormatFasta";
import { GetSeqInfoStream } from "./GetSeqInfoStream";
import { PassCheGeneIdsByCondition } from "./PassCheGeneIdsByCondition";
import { PassGenomeInfoViaMiST3Api } from "./PassGenomeInfoViaMiSTApi";
import {
  cherRegArchRule,
  chewRegArchRule,
  chewrRegArchRule
} from "./regArchDefinitions";

const logger = new Logger("info");
const log = logger.getLogger("CheA-pipeline");

const genomeBufferSize = 100;
const testNumber = 15;

const test = false;

const cheClass = "F2";
const sourceFile = "data/raw/chea.noLineBreaks.json";
const wrGenomeFile = "data/production/wr-pipeline.genome.version.json";
const prefix = "data/production/chea-pipeline";

const outFileGenomesWithWrNotProcessed = `${prefix}.genomesWithWrNotProcessed.json`;

const outFileCheAF2Profile = `${prefix}.profile.F2.table.md`;
const outFileCheAF2Fasta = `${prefix}.chea.F2.fa`;
const outFileCheAF2ChewOnlyFasta = `${prefix}.chea.F2.chew.fa`;
const outFileCheRF2Fasta = `${prefix}.cher.F2.fa`;
const outFileCheRF2CherOnlyFasta = `${prefix}.cher.F2.cher.fa`;
const outFileCheWFasta = `${prefix}.chew.fa`;
const outFileCheWChewOnlyFasta = `${prefix}.chew.chew.fa`;
const outFileCheWRFasta = `${prefix}.chewr.fa`;
const outFileCheWRChewOnlyFasta = `${prefix}.chewr.chew.fa`;
const outFileCheWRCherOnlyFasta = `${prefix}.chewr.cher.fa`;
const outFileReport = `${prefix}.report.json`;

interface IReport {
  totalCheA: number;
  cheAF2: number;
  genomesWithF2: number;
  wrGenomesWithoutCheAF2: string[];
}

const report: IReport = {
  cheAF2: 0,
  genomesWithF2: 0,
  totalCheA: 0,
  wrGenomesWithoutCheAF2: []
};

const wrGenomes: string[] = JSON.parse(
  fs.readFileSync(wrGenomeFile).toString()
) as string[];
log.info(
  `Loaded ${wrGenomes.length} genome versions from wr analysis to check against the chea-pipeline dataset.`
);

const cheaStream = fs.createReadStream(sourceFile);
const parser = StreamArray.withParser();

const main = cheaStream.pipe(parser);

const filterByChe = new FilterGenomesByCheClassStream(cheClass);
const passGenomes = new PassGenomeInfoViaMiST3Api(genomeBufferSize);
const appendCheInfo = new AppendChemosensoryProfile();

filterByChe.on("end", () => {
  report.totalCheA = filterByChe.processed;
  report.cheAF2 = filterByChe.success;
});

passGenomes.on("data", (chunk: { version: string }) => {
  const index = wrGenomes.indexOf(chunk.version);
  if (index !== -1) {
    wrGenomes.splice(index, 1);
    log.info(`Genome ${chunk.version} match. ${wrGenomes.length} to go.`);
  } else {
    log.info(`Genome ${chunk.version} not in the wr analysis.`);
  }
});

passGenomes.on("end", () => {
  report.genomesWithF2 = passGenomes.success;
  if (wrGenomes.length > 0) {
    log.info(
      `There are ${wrGenomes.length} genomes with wr not present in the chea analysis`
    );
    log.info(`They will be posted at: ${outFileGenomesWithWrNotProcessed}`);
    fs.writeFileSync(
      outFileGenomesWithWrNotProcessed,
      JSON.stringify(wrGenomes, null, " ")
    );
  } else {
    log.info(
      `***** All genomes from wr analysis are present in the chea analysis *****`
    );
  }
  report.wrGenomesWithoutCheAF2 = wrGenomes;
});

const buildChemoProfile = new BuildChemoProfile();
const write = fs.createWriteStream(outFileCheAF2Profile);

const appendGtdb = new AppendGtdbInfo();
appendGtdb
  .init()
  .then(() => {
    appendGtdb.on(
      "data",
      (data: { name: string; gtdb: { p: string }; phylum: string }) => {
        try {
          log.info(`Genome ${data.name} is from phylum: ${data.gtdb.p}`);
        } catch (err) {
          log.info(`Genome ${data.name} is from phylum: ${data.phylum}*`);
        }
      }
    );

    const processGenomes = new Pumpify.obj(
      filterByChe,
      passGenomes,
      appendGtdb,
      appendCheInfo
    );

    const fullGenomes = main.pipe(processGenomes);

    fullGenomes.on("end", () => {
      fs.writeFileSync(outFileReport, JSON.stringify(report, null, " "));
    });

    const cheFilterCheaF2 = new PassCheGeneIdsByCondition("chea", cheClass);
    const appendAseq = new AppendAseqInfo();

    const cheAF2Pipeline = new Pumpify.obj(cheFilterCheaF2, appendAseq);
    const chea = fullGenomes.pipe(cheAF2Pipeline);

    const getSeqInfoCheaF2 = new GetSeqInfoStream();
    const formatCheaF2 = new FormatFasta();
    const writeCheAF2Fasta = fs.createWriteStream(outFileCheAF2Fasta);
    const fullCheAF2ToFasta = new Pumpify.obj(
      getSeqInfoCheaF2,
      formatCheaF2,
      writeCheAF2Fasta
    );
    chea.pipe(fullCheAF2ToFasta);

    const getSeqInfoCheaF2ChewOnly = new GetSeqInfoStream("CheW");
    const formatCheaF2ChewOnly = new FormatFasta();
    const writeCheAF2ChewOnlyFasta = fs.createWriteStream(
      outFileCheAF2ChewOnlyFasta
    );
    const fullCheAF2ChewOnlyToFasta = new Pumpify.obj(
      getSeqInfoCheaF2ChewOnly,
      formatCheaF2ChewOnly,
      writeCheAF2ChewOnlyFasta
    );
    chea.pipe(fullCheAF2ChewOnlyToFasta);

    const cheFilterChew = new PassCheGeneIdsByCondition("chew", undefined);
    const appendAseqCheW = new AppendAseqInfo();

    const cheWLikePipeline = new Pumpify.obj(cheFilterChew, appendAseqCheW);
    const chewLike = fullGenomes.pipe(cheWLikePipeline);

    const chewRegArchFilter = new FilterByRegArch(chewRegArchRule);
    const chew = chewLike.pipe(chewRegArchFilter);

    const getSeqInfoChew = new GetSeqInfoStream();
    const formatChew = new FormatFasta();
    const writeCheWFasta = fs.createWriteStream(outFileCheWFasta);
    const fullCheWToFasta = new Pumpify.obj(
      getSeqInfoChew,
      formatChew,
      writeCheWFasta
    );
    chew.pipe(fullCheWToFasta);

    const getSeqInfoChewChewOnly = new GetSeqInfoStream("CheW");
    const formatChewChewOnly = new FormatFasta();
    const writeCheWChewOnlyFasta = fs.createWriteStream(
      outFileCheWChewOnlyFasta
    );
    const fullCheWChewOnlyToFasta = new Pumpify.obj(
      getSeqInfoChewChewOnly,
      formatChewChewOnly,
      writeCheWChewOnlyFasta
    );
    chew.pipe(fullCheWChewOnlyToFasta);

    const chewrRegArchFilter = new FilterByRegArch(chewrRegArchRule);
    const chewr = chewLike.pipe(chewrRegArchFilter);

    const getSeqInfoChewr = new GetSeqInfoStream();
    const formatChewr = new FormatFasta();
    const writeCheWRFasta = fs.createWriteStream(outFileCheWRFasta);
    const fullCheWRToFasta = new Pumpify.obj(
      getSeqInfoChewr,
      formatChewr,
      writeCheWRFasta
    );
    chewr.pipe(fullCheWRToFasta);

    const getSeqInfoChewrChewOnly = new GetSeqInfoStream("CheW");
    const formatChewrChewOnly = new FormatFasta();
    const writeCheWRChewOnlyFasta = fs.createWriteStream(
      outFileCheWRChewOnlyFasta
    );
    const fullCheWRChewOnlyToFasta = new Pumpify.obj(
      getSeqInfoChewrChewOnly,
      formatChewrChewOnly,
      writeCheWRChewOnlyFasta
    );
    chewr.pipe(fullCheWRChewOnlyToFasta);

    const getSeqInfoChewrCherOnly = new GetSeqInfoStream("CheR");
    const formatChewrCherOnly = new FormatFasta();
    const writeCheWRCherOnlyFasta = fs.createWriteStream(
      outFileCheWRCherOnlyFasta
    );
    const fullCheWRCherOnlyToFasta = new Pumpify.obj(
      getSeqInfoChewrCherOnly,
      formatChewrCherOnly,
      writeCheWRCherOnlyFasta
    );
    chewr.pipe(fullCheWRCherOnlyToFasta);

    const cheFilterCherF2 = new PassCheGeneIdsByCondition("cher", cheClass);
    const appendAseqCheR = new AppendAseqInfo();

    const cheRF2Pipeline = new Pumpify.obj(cheFilterCherF2, appendAseqCheR);

    const cherRegArchFilter = new FilterByRegArch(cherRegArchRule);
    const cher = fullGenomes.pipe(cheRF2Pipeline).pipe(cherRegArchFilter);

    const getSeqInfoCherF2 = new GetSeqInfoStream();
    const formatCherF2 = new FormatFasta();
    const writeCheRF2Fasta = fs.createWriteStream(outFileCheRF2Fasta);
    const fullCheRF2ToFasta = new Pumpify.obj(
      getSeqInfoCherF2,
      formatCherF2,
      writeCheRF2Fasta
    );
    cher.pipe(fullCheRF2ToFasta);

    const getSeqInfoCherF2CherOnly = new GetSeqInfoStream("CheR");
    const formatCherF2CherOnly = new FormatFasta();
    const writeCheRF2CherOnlyFasta = fs.createWriteStream(
      outFileCheRF2CherOnlyFasta
    );
    const fullCheRF2CherOnlyToFasta = new Pumpify.obj(
      getSeqInfoCherF2CherOnly,
      formatCherF2CherOnly,
      writeCheRF2CherOnlyFasta
    );
    cher.pipe(fullCheRF2CherOnlyToFasta);

    const buildTable = fullGenomes.pipe(buildChemoProfile).pipe(write);
  })
  .catch((err: string) => {
    throw new Error(err);
  });
