import { Logger, LogLevelDescType } from 'loglevel-colored-prefix';
import { Genome } from 'mist3-ts';
import { Transform } from 'stream';

class PassGenomeInfoViaMiST3Api extends Transform {
  public processed: number;
  public success: number;
  private readonly logLevel: LogLevelDescType;
  private readonly log: any;
  private readonly bufferSize: number;
  private genomeBuffer: any[];
  private readonly genomes: string[];

  public constructor(bufferSize: number = 100, logLevel: LogLevelDescType = 'info') {
    super({ objectMode: true });
    this.bufferSize = bufferSize;
    this.logLevel = logLevel;
    const logger = new Logger(logLevel);
    this.log = logger.getLogger(`PassGenomeInfoViaMiST3Api`);
    this.processed = 0;
    this.success = 0;
    this.genomes = [];
    this.genomeBuffer = [];
  }

  public async _transform(chunk: { stable_id: string }, enc: any, next: () => void) {
    const genome = chunk.stable_id.split('-')[0];
    this.processed++
    if (this.genomes.indexOf(genome) === -1) {
      this.genomeBuffer.push(genome);
      this.genomes.push(genome);
    }
    this.log.debug(`number of genomes on buffer: ${this.genomeBuffer.length}`);
    if (this.genomeBuffer.length === this.bufferSize) {
      const mist3Genome = new Genome('silent');
      this.log.info(`... waiting on MiST3 to return ${this.genomeBuffer.length} genomes ...`)
      const genomeInfoList = await mist3Genome.fetchDetailsMany(this.genomeBuffer);
      genomeInfoList.forEach((genome: any) => {
        this.log.info(`Pushing genome ${genome.name} from strain ${genome.strain}`);
        this.success++
        this.push(genome);
      });
      this.genomeBuffer = [];
    }
    next();
  }

  public async _flush(next: () => void) {
    if (this.genomeBuffer.length) {
      this.log.info(`Getting information of the last ${this.genomeBuffer.length} genomes on MiST3`);
      const mist3Genome = new Genome('silent');
      this.log.info(`... waiting on MiST3 to return ${this.genomeBuffer.length} genomes ...`)
      const genomeInfoList = await mist3Genome.fetchDetailsMany(this.genomeBuffer);
      genomeInfoList.forEach((genome: any) => {
        this.log.info(`Pushing genome ${genome.name} from strain ${genome.strain}`);
        this.success++
        this.push(genome);
      });
    }
    this.log.warn(`Processed: ${this.processed}. Success: ${this.success}`);
    this.genomeBuffer = [];
    next();
  }
}

export { PassGenomeInfoViaMiST3Api };
