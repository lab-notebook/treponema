import { FastaUtils } from "bioseq-ts/lib";
import fs from "fs";

const sourceFile = `data/production/p3-pipeline.chea.P3.F.profile.cd-hit.75.linsi.cleaner.turn.fa`;
const outputFile = `data/production/p3-length.json`;

const msa = fs.readFileSync(sourceFile).toString();

const fu = new FastaUtils();
const bioset = fu.parse(msa);

const counts: Array<{ cheClass: string; value: number }> = [];

bioset.getBioSeqs().forEach(bioSeq => {
  const nogaps = bioSeq.noGaps();
  const cheClass = bioSeq.header.split("-")[3];
  counts.push({
    cheClass,
    value: nogaps.length
  });
});

fs.writeFileSync(outputFile, JSON.stringify(counts));
