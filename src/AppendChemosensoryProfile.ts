import { Logger, LogLevelDescType } from 'loglevel-colored-prefix';
import { Genome } from 'mist3-ts';
import { Transform } from 'stream';

import { ISignalGene } from './interfaces';

class AppendChemosensoryProfile extends Transform {
  public count: number;
  private readonly logLevel: LogLevelDescType;
  private readonly log: any;

  public constructor(logLevel: LogLevelDescType = 'info') {
    super({ objectMode: true });
    this.logLevel = logLevel;
    const logger = new Logger(logLevel);
    this.log = logger.getLogger(`AppendChemosensoryProfile`);
    this.count = 0;
  }

  public async _transform(chunk: { name: string; version: string; cheInfo: any }, enc: any, next: () => void) {
    const version = chunk.version;
    const mist3Genome = new Genome('silent');
    this.log.info(`Waiting for MiST3 to return chemosensory information on ${version}`);
    const chemosensoryInfo = (await mist3Genome.fetchChemotaxis(version)) as ISignalGene[];
    this.log.info(
      `Information received. Genome ${chunk.name} :: ${version} has ${chemosensoryInfo.length} components.`,
    );
    this.log.debug(chemosensoryInfo);
    chunk.cheInfo = chemosensoryInfo;
    this.push(chunk);
    next();
  }
}

export { AppendChemosensoryProfile };
