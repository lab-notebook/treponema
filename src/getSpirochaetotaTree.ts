import { Gtdb } from "gtdb-local";
import { Tree } from "phylogician-ts";

const gtdb = new Gtdb();

const bacTree = gtdb.getBacteriaTree();

const nodeName = "p__Spirochaetota";

const phylo = new Tree();

phylo.buildTree(bacTree);

const spiroNode = phylo.findNodeByExactName(nodeName);
if (spiroNode) {
  console.log(spiroNode)
  const spiroNodeNwk = phylo.writeNewick(spiroNode[0].id);
  const spiro = new Tree();
  spiro.buildTree(spiroNodeNwk);
  const finalNwk = spiro.ladderize(0).writeNewick();
  console.log(finalNwk);
  console.log(`Number of nodes: ${spiro.nodes.length}`);
  console.log(`Number of leafs: ${spiro.getNumberOfLeafs()}`);
}
