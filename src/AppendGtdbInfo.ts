import { Gtdb } from 'gtdb-local';
import { Logger, LogLevelDescType } from 'loglevel-colored-prefix';
import { Transform } from 'stream';

class AppendGtdbInfo extends Transform {
  private readonly logLevel: LogLevelDescType;
  private readonly log: any;
  private readonly gtdb: Gtdb;
  private db: any;

  public constructor(logLevel: LogLevelDescType = 'info') {
    super({ objectMode: true });
    this.logLevel = logLevel;
    const logger = new Logger(logLevel);
    this.log = logger.getLogger(`AppendGtdbInfo`);
    this.gtdb = new Gtdb();
    this.db = undefined;
  }

  public async init() {
    this.log.info(`Connecting to DB`);
    this.db = await this.gtdb.connectDB();
  }

  public async _transform(chunk: { version: string; gtdb: { s: string } | undefined }, enc: any, next: () => void) {
    const version: string = chunk.version;
    const genome = this.ncbi2gtdb(version);
    this.log.debug(`Searching for genome ${genome} on GTDB`);
    if (this.db) {
      await this.db
        .get(genome)
        .then((info: { s: string }) => {
          this.log.info(`Genome found: ${info.s}`);
          chunk.gtdb = info;
          this.push(chunk);
          next();
        })
        .catch((err: any) => {
          this.log.warn(`The ${genome} was not found on gtdb:`);
          this.log.warn(`gtdb-local returned: ${err}`);
          chunk.gtdb = undefined;
          this.push(chunk);
          next();
        });
    } else {
      const errorMsg = `The database is supposed to be connected. Check documentation on how to use this transform stream.`;
      this.log.error(errorMsg);
      this.destroy()
      throw new Error(errorMsg);
    }
  }

  private ncbi2gtdb(version: string) {
    if (version.match(/^GCF_/)) {
      return `RS_${version}`;
    } else if (version.match(/^GCA_/)) {
      return `GB_${version}`;
    }
    return version;
  }
}

export { AppendGtdbInfo };
