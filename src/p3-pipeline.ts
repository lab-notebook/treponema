import fs, { write, WriteStream } from "fs";
import { Logger } from "loglevel-colored-prefix";
import Pumpify from "pumpify";
// tslint:disable-next-line: no-submodule-imports
import StreamArray from "stream-json/streamers/StreamArray";
import through2 from "through2";

import { FilterGenesByCheStream } from "./FilterGenesByCheStream";
import { FormatFasta } from "./FormatFasta";
import { GetSeqInfoStream } from "./GetSeqInfoStream";

const logger = new Logger("info");
const log = logger.getLogger("P3-pipeline");

const domainBoundariesTolerance = 0;

const sourceFile = "data/raw/chea.noLineBreaks.json";
const prefix = "data/production/p3-pipeline";

const outFileCheAP3Fasta = (cheClass: string) =>
  `${prefix}.chea.P3.${cheClass}.fa`;
const outFileReport = `${prefix}.report.json`;

interface IReport {
  totalCheAs: number;
  cheAsWithP3: number;
}

const report: IReport = {
  cheAsWithP3: 0,
  totalCheAs: 0
};
const cheaStream = fs.createReadStream(sourceFile);
const parser = StreamArray.withParser();

let count = 0;

const parseData = through2.obj(function(chunk, enc, next) {
  const newChunk = chunk.value;
  newChunk.cheClass = newChunk.ranks[2];
  newChunk.cheType = newChunk.ranks[1];
  log.debug(newChunk.cheClass);
  count++;
  this.push(newChunk);
  if (count % 1000 === 0) {
    log.info(`progress: ${count}`);
  }
  next();
});

const cheClasses: string[] = [];
const outputStreams: { [cheClass: string]: WriteStream } = {};

const writeDownPerClass = through2((chunk: { header: string }, enc, next) => {
  const cheClass = chunk
    .toString()
    .split("\n")[0]
    .split("-")[3];
  if (cheClasses.indexOf(cheClass) === -1) {
    cheClasses.push(cheClass)
    const filename = outFileCheAP3Fasta(cheClass);
    outputStreams[cheClass] = fs.createWriteStream(filename);
  }
  outputStreams[cheClass].write(chunk);
  next();
});

const trimP3 = new GetSeqInfoStream(
  "H-kinase_dim",
  domainBoundariesTolerance,
  undefined,
  "warn"
);

trimP3.on("end", () => {
  report.totalCheAs = trimP3.processed;
  report.cheAsWithP3 = trimP3.success;
  fs.writeFileSync(outFileReport, JSON.stringify(report, null, " "));
});

const makeFasta = new FormatFasta("WARN");

const main = new Pumpify.obj(
  parser,
  parseData,
  trimP3,
  makeFasta,
  writeDownPerClass
);

cheaStream.pipe(parser);

