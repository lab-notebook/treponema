import { Logger, LogLevelDescType } from "loglevel-colored-prefix";
import { Transform } from "stream";

class FilterGenesByCheStream extends Transform {
  public processed: number;
  public success: number;
  private readonly cheClass: string | undefined;
  private readonly cheType: string | undefined;
  private readonly logLevel: LogLevelDescType;
  private readonly log: any;

  public constructor(
    cheClass: string | undefined,
    cheType: string | undefined,
    logLevel: LogLevelDescType = "info"
  ) {
    super({ objectMode: true });
    this.cheClass = cheClass;
    this.cheType = cheType;
    this.logLevel = logLevel;
    const logger = new Logger(this.logLevel);
    this.log = logger.getLogger(`FilterGenesByCheStream-${cheClass}`);
    this.processed = 0;
    this.success = 0;
  }

  public _transform(
    chunk: { cheClass: string; cheType: string; stable_id: string },
    enc: any,
    next: () => void
  ) {
    this.log.info(chunk)
    this.processed++;
    if (this.cheClass) {
      if (chunk.cheClass !== this.cheClass) {
        next();
        return;
      }
    }
    if (this.cheType) {
      if (chunk.cheType !== this.cheType) {
        next();
        return;
      }
    }
    this.success++;
    this.log.info(`${this.success}/${this.processed} - ${chunk.stable_id}`);
    this.push(chunk);
    next();
  }

  public _flush(next: () => void) {
    this.log.warn(`Processed: ${this.processed}. Success: ${this.success}`);
    next();
  }
}

export { FilterGenesByCheStream };
