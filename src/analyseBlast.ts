import fs from 'fs';
import { Logger } from 'loglevel-colored-prefix';
import through2 from 'through2';

const blastFile = 'test.blastp.dat';
const blastOut = 'test.blastp.json';

const logger = new Logger('info');
const log = logger.getLogger('a-blast');

const rawStream = fs.createReadStream(blastFile);
const writeDown = fs.createWriteStream(blastOut);

let parseLineBuffer = Buffer.from('');

const parseLines = through2(
  function(chunk, enc, next) {
    parseLineBuffer += chunk;
    const lines = parseLineBuffer.toString().split('\n');
    const lastLine = lines.pop();
    if (lastLine) {
      parseLineBuffer = Buffer.from(lastLine);
    }
    lines.forEach(line => this.push(line));
    next();
  },
  function(next) {
    const lines = parseLineBuffer.toString().split('\n');
    lines.forEach(line => this.push(line));
    next();
  },
);

let first = true;
const parseResults = through2(
  function(chunk, env, next) {
    const fields = chunk.toString().split('\t');
    if (first) {
      this.push('[\n');
      first = false
    } else {
      this.push(',\n');
    }
    this.push(
      JSON.stringify({
        q: fields[0],
        h: fields[1],
        logEv: -Math.log10(parseFloat(fields[10])).toFixed(2),
        s: parseFloat(fields[2]),
      }),
    );
    next();
  },
  function(next) {
    this.push('\n]\n');
  },
);

rawStream
  .pipe(parseLines)
  .pipe(parseResults)
  .pipe(writeDown);
