import { FastaUtils } from "bioseq-ts";
import fs from "fs";
import { Logger } from "loglevel-colored-prefix";
import { Genes, Genome } from "mist3-ts";

const stableIdTagSliceLimit = 2;

const prefix = "data/production/classify-w";

const chewFasta = `data/production/chea-pipeline.chew.cd-hit.1.fa`;
const geneHoodFile = `${prefix}.genehood.json`;
const outFileJson = `${prefix}.nonRedundant.json`;
const outFileCheProfileDb = `${prefix}.cheProfile.json`;

const logger = new Logger("info");
const log = logger.getLogger("classify-w");

const fastaFile = fs.readFileSync(chewFasta).toString();
const geneHoodData = fs.existsSync(geneHoodFile)
  ? JSON.parse(fs.readFileSync(geneHoodFile).toString())
  : [];
const genomeChemoProfileDb = fs.existsSync(outFileCheProfileDb)
  ? JSON.parse(fs.readFileSync(outFileCheProfileDb).toString())
  : [];

const fu = new FastaUtils();
const chewSet = fu.parse(fastaFile);

const headers = chewSet.getBioSeqs().map(bioseq => bioseq.header);

const markW: Array<{ h: string; classLike: string; new?: boolean }> = [];

const getHood = async (header: string, d: number, u: number) => {
  const mist3genes = new Genes("warn");
  const stableId = header
    .split("-")
    .slice(0, stableIdTagSliceLimit)
    .join("-");
  log.info(`Fetching gene neighborhood for ${stableId}`)
  return mist3genes.getGeneHood(stableId, d, u);
};

const getCheProfile = async (version: string) => {
  const mist3genomes = new Genome("warn");
  log.info(`Going to get a chemoProfile from ${version}, be right back...`);
  const profile = await mist3genomes.fetchChemotaxis(version);
  return {
    profile,
    version
  };
};

const appendGeneHoodDb = async (header: string, up: number, down: number) => {
  const hood = await getHood(header, up, down);
  geneHoodData[header] = hood;
  log.info(`Updating the file DB`);
  fs.writeFileSync(geneHoodFile, JSON.stringify(geneHoodData));
};

const analyseCheWF2s = async (up: number, down: number) => {
  for (let j = 0; j < headers.length; j++) {
    const header = headers[j];
    log.info(`let's do this thing:`);
    log.info(header);
    const version: string = header.split("-")[0];
    let chemoProfile = genomeChemoProfileDb.find(
      (g: { version: string }) => g.version === version
    );
    if (!chemoProfile) {
      chemoProfile = await getCheProfile(version);
      genomeChemoProfileDb.push(chemoProfile);
    }
    log.info(
      `Found chemoprofile of this genome. It has ${
        chemoProfile.profile.length
      } genes. ${j + 1}/${headers.length}`
    );
    const cheas = chemoProfile.profile.filter(
      (g: { ranks: string[] }) => g.ranks[1] === "chea"
    ) as Array<{ ranks: string[] }>;
    log.info(cheas.map(c => c.ranks));
    if (cheas.length === 1) {
      if (cheas[0].ranks[2] === "F2") {
        log.warn(`Found one: ${header}`);
        markW.push({
          classLike: "F2",
          h: header
        });
      } else {
        const classLike = cheas[0].ranks[2]
        markW.push({
          classLike,
          h: header,
          new: true
        });
      }
    } else {
      let geneHood = geneHoodData[header];
      log.info(
        `checking if record needs update and updating if needs. ${j + 1}/${
          headers.length
        }`
      );
      if (!geneHood) {
        log.info(`There is no record, let's grab it.`);
        await appendGeneHoodDb(header, up, down);
      } else if (geneHood.length < up + down + 1) {
        log.info(
          `We must update the GeneHood DB, this has a smaller boundaries than what we want: ${geneHood.length}`
        );
        await appendGeneHoodDb(header, up, down);
      }
      geneHood = JSON.parse(JSON.stringify(geneHoodData[header]));
      if (geneHood.length > up + down + 1) {
        log.info(
          `The genehood data for ${header} has ${geneHood.length} elements.`
        );
        const refIndex: number = geneHood
          .map((g: { stable_id: any }) => g.stable_id)
          .indexOf(header);
        if (refIndex === -1) {
          log.error(
            `This is unexpected. It should have ${header} in the geneHood.`
          );
          log.error(geneHood);
        }
        const lowerBound = refIndex - down;
        const upperBound = refIndex + up;
        geneHood.splice(lowerBound, upperBound);
      }
      log.info(
        `The genehood data for ${header} has ${geneHood.length} elements.`
      );
      if (geneHood.length !== up + down + 1) {
        log.warn(
          `This is unexpected but it could happen that this is the end of the chromosomal components. It should have ${up +
            down +
            1} elements.`
        );
      }
      geneHood.forEach((gene: { id: any }) => {
        const cheProf = chemoProfile.profile.find(
          (che: { gene_id: any }) => che.gene_id === gene.id
        );
        if (cheProf) {
          if (cheProf.ranks[1] === "chea" && cheProf.ranks[2] !== "F2") {
            log.warn(
              `Found one of another class: ${header} - ${cheProf.ranks[2]}`
            );
            markW.push({
              h: header,
              classLike: cheProf.ranks[2]
            });
          }
        }
      });
    }
  }
  fs.writeFileSync(outFileCheProfileDb, JSON.stringify(genomeChemoProfileDb));
  fs.writeFileSync(outFileJson, JSON.stringify(markW, null, " "));
  log.info("All done!");
};

analyseCheWF2s(5, 5).catch(err => {
  throw err;
});
