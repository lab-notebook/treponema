import { BioSeqSet, FastaUtils } from "bioseq-ts";
import fs from "fs";
import { Logger } from "loglevel-colored-prefix";

const chewFasta = "data/production/chea-pipeline.chew.cd-hit.1.fa"; // 'mark-w.test.fa' //
const outFileFilteredFastaF2 =
  "data/production/chea-pipeline.chew.cd-hit.1.F2.fa";
const candidateList = "data/production/predictedChewF2ListHeaders.json";

const logger = new Logger("info");
const log = logger.getLogger("filterSequences");

const fastaFile = fs.readFileSync(chewFasta).toString();
const candidates = JSON.parse(
  fs.readFileSync(candidateList).toString()
) as string[];

const fu = new FastaUtils();
const chewSet = fu.parse(fastaFile);

const cheWF2 = chewSet
  .getBioSeqs()
  .filter(seq => candidates.indexOf(seq.header) !== -1);
log.info(
  `There are ${cheWF2.length} selected CheW sequences. It should have ${candidates.length}`
);
const cheWF2Set = new BioSeqSet(cheWF2);

fs.writeFileSync(outFileFilteredFastaF2, fu.write(cheWF2Set));
log.info(`Sequences written in: ${outFileFilteredFastaF2}`);
