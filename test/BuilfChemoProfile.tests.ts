import { expect } from 'chai';
import { LogLevelDescType } from 'loglevel-colored-prefix';

import { BuildChemoProfile } from '../src/BuildChemoProfile';

const level = 'debug';

describe('BuildChemoProfile', () => {
  describe('sortHeader()', () => {
    class MockBuilder extends BuildChemoProfile {
      public constructor(logLevel: LogLevelDescType) {
        super(logLevel);
      }
      public sortHeader() {
        return super.sortHeader();
      }

      public setHeader(newHeader: string[]) {
        this.headerTypes = newHeader;
      }

      public getHeader() {
        return this.headerTypes;
      }
    }
    it('should sord a header just fine', () => {
      const unsortedHeader = [
        'cheb-ACF',
        'chea-ACF',
        'cher-ACF',
        'cheb-F1',
        'ched-F1',
        'chea-F2',
        'cheb-F2',
        'cher-F2',
        'chev-F3',
        'cher-F5',
        'cheb-F5',
        'chea-F5',
        'cher-F7',
        'ched-F7',
        'chea-F7',
        'cheb-F7',
        'ched-F8',
        'cheb-F8',
        'chea-F8',
        'cher-F8',
        'cheb-MAC1',
        'cher-MAC2',
        'cheb-MAC2',
        'cher-Uncat',
        'checx-undefined',
        'chew-undefined',
        'mcp-24H',
        'mcp-28H',
        'mcp-34H',
        'mcp-36H',
        'mcp-38H',
        'mcp-40H',
        'mcp-44H',
        'mcp-48H',
        'mcp-52H',
        'mcp-58H',
        'other-undefined',
      ];
      const expectedHeader = [
        'chea-ACF',
        'cheb-ACF',
        'cher-ACF',
        'cheb-F1',
        'ched-F1',
        'chea-F2',
        'cheb-F2',
        'cher-F2',
        'chev-F3',
        'chea-F5',
        'cheb-F5',
        'cher-F5',
        'chea-F7',
        'cheb-F7',
        'cher-F7',
        'ched-F7',
        'chea-F8',
        'cheb-F8',
        'cher-F8',
        'ched-F8',
        'cheb-MAC1',
        'cheb-MAC2',
        'cher-MAC2',
        'cher-Uncat',
        'checx-undefined',
        'chew-undefined',
        'mcp-24H',
        'mcp-28H',
        'mcp-34H',
        'mcp-36H',
        'mcp-38H',
        'mcp-40H',
        'mcp-44H',
        'mcp-48H',
        'mcp-52H',
        'mcp-58H',
        'other-undefined',
      ];
      const b = new MockBuilder(level);
      b.setHeader(unsortedHeader);
      b.sortHeader();
      const sortedHeader = b.getHeader();
      expect(sortedHeader).eql(expectedHeader);
    });
  });
});
