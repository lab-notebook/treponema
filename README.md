# Treponema

A notebook of project in collaboration with the Briegel Lab.

> This is an experiment to publish all notes. You will find all scripts used in manuscript and instructions on how to run it, albeit imperfect in some cases. If you have questions, please open an issue or contact me (Davi).

## How to reproduce the data

### Initial Datasets

The raw dataset used are two database dumps from MiST3 containing all records of (1) cheA and (2) CheB. The dump is a join between the tables "signal_genes", "genes" and "seqdepot.aseqs" in JSON format.

These initial datasets are too large for the repositories, please contact us and we will send them to you.

### Dependencies

To run the scripts we only need to have NodeJS and an internet connection. Here are some solutions to install NodeJS:

[Linux, MacOS](https://github.com/nvm-sh/nvm)
[Windows](https://github.com/coreybutler/nvm-windows)

The source code can be cloned from the repository using git. Instructions on how to install git can be found [here](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

### Install

The scripts uses a range of modules. To install them:

```bash
npm install
```

> If you don't have the source code from the zip file, you need to clone the repository:

In Linux/MacOS:

```bash
git clone https://gitlab.com/lab-notebook/treponema.git
cd treponema`
```

### How to reproduce the data?

To run these scripts, we use `npm` (already installed on NodeJS). To run a script with `npm` is simple:

`npm run name-of-the-script`

Some scripts generates files that will be used by others. This is a recommended order:

```bash
npm run spiro-gen
npm run wr-pipeline
npm run spiro-pipeline
npm run chea-pipeline
```

There are other scripts in this package, but they were used to explore the data and to produce other parts of the dataset, see the description of the scripts below.

#### Classify CheW

Dependencies:

* RAxML
* MAFFT
* TreeCollapseCL4
* CD-HIT


##### Step 1: Remove identical sequences using cd-hit.

> You need to have CD-HIT on your machine.

```bash
cd data/production
cd-hit -i chea-pipeline.chew.fa -o chea-pipeline.chew.cd-hit.1.fa -c 1
```

##### Step 2: Run `classify-w`

```bash
npm run classify-w
```

##### Step 3: Align them using MAFFT's L-INS-I algorithm:

> The flag `--thread` takes advantage of a multi-core CPUs.

```bash
linsi --thread 12 chea-pipeline.chew.cd-hit.1.fa > chea-pipeline.chew.cd-hit.1.linsi.fa
```

##### Step 4: Make a phylogenetic tree of the sequences using RAxML.

> This step will take a while

```bash
raxmlHPC-PTHREADS-AVX -T 40 -m PROTGAMMAILG -p 1234555 -x 9876545 -f a -N 1000 -s chea-pipeline.chew.cd-hit.1.linsi.fa -n chea-pipeline.chew.cd-hit.1.linsi.nwk
```

##### Step 5: Collapse the tree at 50% bootstrap confidence score.

```bash
java -jar TreeCollapseCL4.jar -b 50 -f RAxML_bipartitions.chea-pipeline.chew.cd-hit.1.linsi.nwk
```

> Note 1: TreeCollapse is a third party software and should a full path must be passed to it.

##### Steps 6,7,8,9: ObservableHQ

Please go to [this observableHQ](https://observablehq.com/d/50ee213acd2207e0) to continue with the analysis.

##### Step 10: Filter CheW-F2 candidate sequences.

```bash
npm run filter-chew-f2
```

##### Final remark

Now the file chea-pipeline.chew.cd-hit.1.F2.fa contain a non-redundant set of CheW-F2 candidates.


### Descriptions of relevant scripts

#### spiro-gen

It generates the list of genome versions that are classified as Spirochetota and also generates a list of these genomes that are present in MiST3 as well.

#### spiro-pipeline

It reads the list of genome information provided by `spiroGenerateGtdbGenomeList.ts` and builds chemosensory profile table of all Spirochaetota in MiST3.

#### chea-pipeline

It builds the all fasta files and the table with the complete chemosensory profile of all genomes with CheA-F2 systems.

#### wr-pipeline

Produce a list of genomes containing CheWR genes.

#### classify-w

It classify CheWs as described in the Materials and Methods. In a nutshell, it associates with CheW with F2 systems when they are present in a genome with no other chemotaxis system. It also associates to other systems if it is found on the genome within 5 genes away from a CheA.

It reads a manually built CheW alignment placed in the `production` directory with the name `chea-pipeline.chew.cd-hit.1.fa`. It is just the fasta file produced by `chea-pipeline` with cd-hit to remove identical sequences.

#### spiro-tree

This script will use [gtdb-local](https://www.npmjs.com/package/gtdb-local) to get the sub-tree of Spirochaetota from [GTDB](https://gtdb.ecogenomic.org/) and output the tree in the console.


### Descriptions of some files in `/assets`

|Filename | Description|
|-|-|
|`keepFromReference.production.chea-pipeline.cher.F2.linsi.fa`| full length alignment of the CheR sequences used to build the sequence logos |
|`keepFromReference.production.chea-pipeline.chewr.linsi.fa`| full length alignment of the CheWR sequences used to build the sequence logos |

### Datasets legends

|file|legend|
|-|-|
|Dataset1.xlsx| Distribution of chemosensory proteins in per genome, per class or genomes with at least 1 CheA-F2. Each element of the table shows the total number of proteins in that genome (number inside brackets) and the MiST3 ids of the proteins (between parenthesis).|
|Dataset2.xlsx| Distribution of chemosensory proteins in Spirochaetotas per genome, per class. Each element of the table shows the total number of proteins in that genome (number inside brackets) and the MiST3 ids of the proteins (between parenthesis).|