I am going to answer them in line here:

6)Lines 76-79. How many of the ‘Flagellar’ systems are of the F2 type? Only one? Are there other types?

We are sorry for the confusion. There are 17 classes of chemotaxis system originally predicted to control flagellar motility, thus the name "Flagellar" and the letter F. Althought this prediction no longer holds, the name of the classes remains.

We changed the text from:
"The chemotaxis systems in prokaryotes have been classified into 19 systems based on phylogenomic markers [14]. These classes include 17 ‘Flagellar’ systems (F), one ‘Alternative Cellular Function’ system (ACF), and one ‘Type Four Pilus system’ (TFP). The spirochete chemotaxis system belongs to the ‘Flagella class 2’ (F2) category, which has not been investigated with structural methods".

To

"The chemotaxis systems in prokaryotes have been divided into 19 classes based on phylogenomic markers [14]. These classes include 17 classes predicted to control ‘Flagellar motility’ (F1-17), one to control ‘Alternative Cellular Function’ (ACF), and one to control ‘Type Four Pilus systems’ (TFP). The spirochete chemotaxis system belongs to the F2 category, which has not been investigated with structural methods"

The question of how many systems are of the type F2 is relative based on the gebnomes present in MiST3 database. We can confidently mention the 306 CheA-F2 present in MiST3 and we made the following changes:

In the methods section, we changed the segment:

"In MiST3[12], 306 genomes contain at least 1 CheA-F2 (Dataset 1) with three exceptions:"

to:

"Although we selected genomes from the MiST3[12] database that contain at least 1 CheA-F2, we found no genomes with more than 1 CheA-F2 per genome, thus there are 306 F2 systems in the MiST3 database. All 306 genomes are Spirochaetota with three exceptions:"

8)Line 84. Insert comma after “GTDB,” and explain what GTDB is.

We changed the segment:

"There are 1096 genomes assigned to the Spirochaetota phylum in GTDB[16]"

to:

"There are 1096 genomes assigned to the Spirochaetota phylum in the Genome Taxonomy Database (GTDB) [16], a microbial taxonomy based in genome phylogeny." (Alise, this is directly from the website... not sure how plagiarism work here, but they nailed that description and I don't think I can do better).


11)Line 114. The term “location of the last common ancestor” (add “the”) is vague. “location on the CheW phylogenetic tree” would be more informative. Also, I find the legend to Figure S4 inadequate. Here again, the question arises of how many F types there are. (See comment on lines 76-79.) Also, explain all of what is shown in the figure more clearly. It seems that all of the known CheW sequences are mapped.


Thank you for calling our attention to this. We made the following changes to clarify our text:


For the sake of accuracy, we changed the segment:

"F2 systems contain three proteins with a CheW domain: the classical scaffold CheW, CheW113 CheRlike, and the P5 domain from the histidine kinase CheA."


to:


"F2 systems contain three proteins with a CheW domain: the classical scaffold CheW, CheW113 CheRlike, and the histidine kinase CheA."


We also decided to remove the sentence mentioned by the reviewer:


"Phylogenetic mapping of CheW-F2 proteins indicate the location of last common ancestor (Fig. S4, see 115 Supplementary methods)"


In the final text, this is not necessary as the identification of the last common ancestor of the CheW-F2 among other CheW proteins in these organisms is simply an accessory analysis to confidently identify CheW from the F2 class. This is a necessary step as there is no hidden markov model to classify CheW into chemotaxis classes. This was better explained in the methods section "Classification of CheW".


We changed the header "Comparison of the CheW domains in CheA, CheW-CheRlike and CheW" to:


"Comparison of the CheW domains in CheA-F2, CheW-CheRlike and CheW-F2"


For coherence, we also changed the main text segment:


"To investigate sequence patterns of the three CheW domains, we analyzed non-redundant sequence datasets of CheW, CheW-CheRlike, and CheA-F2 from all 117 genomes with at least one CheA-F2. The final alignments for each group contain the CheW domain portion of 74 CheW proteins, 59 CheW-CheRlike proteins, and 73 CheA proteins."


to


"To investigate sequence patterns of the three CheW domains, we analyzed non-redundant sequence datasets of CheW-F2, CheW-CheRlike, and CheA-F2 from all 117 genomes with at least one CheA-F2. The final alignments for each group contain the CheW domain portion of 74 CheW-F2 proteins, 59 CheW-CheRlike proteins, and 73 CheA-F2 proteins."


Finally, the reviewer is correct to assume we changed the legend of Figure S4 from:

"Phylogenetic tree of CheW sequences in genomes with at least one CheA-F2 suggests a last common ancestor of CheW-F2 sequences. We mapped the sequences of CheW from the classes F2 (red), F5(purple), F7 (light green), F8 (green) and ACF (blue). The larger red internal node marks a candidate of the last common ancestor of CheW-F2."

to

"Phylogenetic tree of a non-redundant set of CheW protein sequences in genomes with at least one CheA-F2. The initial classification of CheW classes are mapped to the tree nodes: F2 (red), F5(purple), F7 (light green), F8 (green) and ACF (blue). The clustering of the CheW classified as class F2 suggests a last common ancestor of CheW-F2 sequences (larger red internal node). This clustering allows us to extrapolate the conservative selection of F2 sequences to other CheW proteins in the cluster to get a more inclusive set."




On 06/07/2020 08:04, Muok, A.R. wrote:
>
> Hi Davi,
>
>
> We did all experiments for the revision so I'm going to try and wrap it all up to submit within 2 weeks. Can you answer some of the questions about your section below?
>
>
> 6)Lines 76-79. How many of the ‘Flagellar’ systems are of the F2 type? Only one? Are there other types?
>
>
>
>
> 8)Line 84. Insert comma after “GTDB,” and explain what GTDB is.
>
>
> 11)Line 114. The term “location of the last common ancestor” (add “the”) is vague. “location on the CheW phylogenetic tree” would be more informative. Also, I find the legend to Figure S4 inadequate. Here again, the question arises of how many F types there are. (See comment on lines 76-79.) Also, explain all of what is shown in the figure more clearly. It seems that all of the known CheW sequences are mapped.
>
>
>
-- 

Davi Ortega, Ph.D. | Briegel Lab | Leiden University
Sylviusweg 72, 2333 BE Leiden, The Netherlands

