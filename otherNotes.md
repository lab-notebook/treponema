# Open notebook

This is an experiment to publish notebook, incomplete and likely incorrect some times, notebook notes taken while working on this project.

## Initial scope

I was asked to work with [_Treponema denticola_](https://mistdb.com/genomes/GCF_000008185.1) - GCF_000008185.1 to describe CheW-CheR protein.

## Notebook

### Summary of results

There are 274 CheWR in the MiST3 database. From those 2 of them come from the same genome. Most of these genomes also contain F2 systems (257) with the exception of 16 incomplete genomes (16). 

List of the 16 incomplete genomes, 15 at contig assembly level and 1 (GCF_000413035.1) at scaffold level) with CheWR but not CheA-F2.

```javascript
[  
  'GCF_000501055.1', 'GCF_000501015.1',
  'GCF_000501175.1', 'GCF_000501615.1',
  'GCF_000501235.1', 'GCF_000501195.1',
  'GCF_000501815.1', 'GCF_000465015.1',
  'GCF_000465075.1', 'GCF_002916695.1',
  'GCF_000171775.1', 'GCF_000413035.1',
  'GCF_000501315.1', 'GCF_000501335.1',
  'GCF_000502135.1', 'GCF_000502035.1'
]
```

It is safe to assume that CheWR is unique to F2 systems as predicted by Wuichet and Zhulin 2010. 

We expand our analysis to all genomes with at least 1 CheA-F2. There are 306 genomes with CheA-F2. From those, only 49 did not contain CheWR. These genomes with CheA-F2 genes but without CheWR are also missing other F2 components (look at the table and make sure to describe this carefully).

The next step is to find out if all Spirochaetotas genomes with chemosensory systems have F2 systems. There are 1096 genomes in the GTDB classified as Spirochaetota but only 814 are present in MiST3 and 804 had at least 1 CheA.

We map the distribution of CheA-F2 in the Spirochaetota tree according to GTDB.

### Analysis of CheW domains

We started from the full length CheW (598) sequences selected by the pipeline. We used CD-HIT to remove 405 redundant sequences (-c 1). We aligned the remaining sequences (193) with L-INS-I algorithm of the MAFFT package. Next we produce a phylogeny with 1000 rapid bootstrap using RAxML (-f a -m PROTGAMMAIAUTO -N 1000) and collapsed the phylogeny using a java script TreeCollapse4 to generate the CheW tree in Figure SXX-chew-tree.

The protein CheW has no classification on MiST. To mark candidate CheWs for the F2 system, we marked CheW proteins present in genomes with only 1 chemosensory system: the F2 system - marked by having only 1 CheA of the class F2. We pass all the CheW sequences we previously selected through a pipeline that first search for the CheW gene neighbohood (five genes up and five genes downstream), then it searches the chemotaxis profile of the genome it belongs to check if there is a CheA-F2 and if it is one of the cheW's neighboring genes. This pipeline builds a list of all CheW neighbohring a CheA-F2. We mapped these CheW to the tree (Figure SXX-chew-tree).

Contrary to the _cheW_ gene in F2 systems, _cheW_ genes of other systems often cluster with other components. As a negative control, we also marked the CheW sequences from genes present within 5 genes away from a _cheA_ of any class but F2.

We proposed the node marked as a large red circle as the last common ancestor of the CheW-F2 in this analysis and collect all the 74 sequences from this clade. To compare with other CheW domains in CheA and CheWR.

![](assets/FigureSX-chew-tree.png)
> Figure-SXX-chew-tree. Phylogenetic inference of CheW sequences in genomes with F2 systems. Sequences of CheW in genomes with only one chemosensory system F2 are marked in red. Sequences that are present in genomes with more than 1 system and within 5 genes from a _cheA_ gene are assigned to the corresponding CheA class ACF (purple), F5(blue), F7 (light green) and F8 (green). Based on the distribution of the condidates of CheW-F2 (marked in red) we proposed that the last common ancestor of of CheW-F2 is the internal node (larger red node).

### Comparison of the CheW domains in CheA, CheWR and CheW.

We put together the sequences from CheA-F2 and CheWR (both trimmed by the PFAM model for the CheW domain) and the full sequence of the CheW-F2 selected in the previous step. We then use L-INS-I algorithm to align the sequences and Jalview to manually inspect and eliminate identical reduntant sequences. Finally, we trimmed the whole alignment based on the boudaries of CheA-F2 and CheWR and eliminated one incomplete sequence: GCF_000413015.1-HMPREF1221_RS07250. The final alignment had a total of 206 sequences: 73 CheA, 59 CheWR and 74 CheW. We separated the alignments in individual files and build sequence logos to sumarize the positional diversity of each group, Figure-SXX-chew-comparison.

![](assets/FigureSX-chew-comparison.png)
> Figure-SXX-chew-comparison. Sequence logo of representative sequences of the three groups of CheW-conatining proteins in the F2 system.

## Comparison of CheR domains
First we add together the trimmed part matching the CheR domain of the CheWR sequences and the 292 sequences of CheR protein. We aligned the 550 sequences using L-INS-I. We used Jalview to inspect the alignment and remove identical redundant sequences. The final alignment was separated in two file formats, one for each protein to generate independent sequence logos, Figure-SXX-cher-comparison.

![](assets/FigureSX-cher-comparison.png)
> Figure-SXX-cher-comparison. Sequence logo of representative sequences of the two groups of CheR-conatining proteins in the F2 system.

### Preparation of alignments for consurf. (post-submission to Nat Comms. - prior to revisions)

We will select the full-length sequences used to build the logos to build alignments for consurf. For that we used a custom script (`bitkTS-keepFromReference`) to keep only sequences that were present in the final set used in the sequence logos.





## Analysis of P3 domain.

The `chea-pipeline` in version `2.1.0` already parses all CheA-F2 with domain architecture present which allow us to extract P3. We built a new pipeline that extracts the P3 domain from all CheA sequences and separate them in different classes: `p3-pipeline`. For each flagellar class, we decrease redundancy using cd-hit with 75% identity threshold.

```bash
for i in `ls p3-pipeline.chea.P3.F*.fa`; do cd-hit -i ${i} -o ${i/.fa/.cd-hit.75.fa} -c 0.75; done
```

Then we aligned the remainder sequences using L-INS-I.

```bash
for i in `ls p3-pipeline.chea.P3.F*cd-hit.75.fa`; do mafft --maxiterate 1000 --localpair --thread 7 ${i} > ${i/.fa/.linsi.fa}; done
```

We examined each individual alignment manually with Jalview and eliminated divergent sequences that opens major gaps. Next, we use `mafft-profile` to align the profiles:

```bash
mafft --thread 6 --maxiterate 1000 --seed p3-pipeline.chea.P3.F1.cd-hit.75.linsi.cleaner.fa --seed p3-pipeline.chea.P3.F2.cd-hit.75.linsi.cleaner.fa --seed p3-pipeline.chea.P3.F3.cd-hit.75.linsi.cleaner.fa --seed p3-pipeline.chea.P3.F4.cd-hit.75.linsi.cleaner.fa --seed p3-pipeline.chea.P3.F5.cd-hit.75.linsi.cleaner.fa --seed p3-pipeline.chea.P3.F6.cd-hit.75.linsi.cleaner.fa --seed p3-pipeline.chea.P3.F7.cd-hit.75.linsi.cleaner.fa --seed p3-pipeline.chea.P3.F8.cd-hit.75.linsi.cleaner.fa --seed p3-pipeline.chea.P3.F9.cd-hit.75.linsi.cleaner.fa --seed p3-pipeline.chea.P3.F10.cd-hit.75.linsi.cleaner.fa --seed p3-pipeline.chea.P3.F11.cd-hit.75.linsi.cleaner.fa --seed p3-pipeline.chea.P3.F12.cd-hit.75.linsi.cleaner.fa --seed p3-pipeline.chea.P3.F13.cd-hit.75.linsi.cleaner.fa --seed p3-pipeline.chea.P3.F15.cd-hit.75.linsi.cleaner.fa --seed p3-pipeline.chea.P3.F16.cd-hit.75.linsi.cleaner.fa /dev/null > p3-pipeline.chea.P3.F.profile.cd-hit.75.linsi.cleaner.fa

```

This alignment does not work very well, the insertions in the P3 domain does not seem to be related between classes. We measure the number of residues between the two conserved parts of the P3 domain per class and show in Figure SXX-p3-length.

![](assets/FigureSX-p3-length.png)
Figure SX-p3-length: The class F2 has a longer P3 than other classes. Points are transparent, the darker the point the higher the number of representatives with that value. Numbers on top of the distributions are the sample size of each class.

### Exploration strategy

The idea is to first collect the MiST information for all CheWs (and CheAs) and search for CheW-CheRs (CheWR) and CheR-CheWs (CheRW) using RegArch.

Then we need to answer a few questions:

#### Are these proteins aways with CheA-F2 and Spirochaetota only?

To approach this we will make a table of presence and abscence of CheA-F2 and CheRWs/CheWR in genomes with either proteins. We will also include the presence of other classes of CheAs.

We will also map these proteins into a simplified version of the GTDB to show the distribution on the tree of life.

#### Is there a difference between CheR-CheW and CheW-CheR?

#### What is the origin of CheR-CheW?

#### Correlation of CheR-CheW/F2 and high curvature bacteria.

#### Correlation of P3 extension and CheR-CheW/F2 and high curvature bacteria

#### What is the function of CheR-CheW?

#### Does CheR-CheW bind the receptor's methylation sites?

### General Notes

#### Collecting CheWs from MiST postgresql database

psql query: `\COPY (SELECT json_agg(row) FROM (signal_genes s INNER JOIN genes g ON s.gene_id = g.id INNER JOIN seqdepot.aseqs a ON a.id = g.aseq_id) row WHERE 'chew' = ANY (ranks) ) to './chew.json'`

This query contains the `pfam31` annotation that RegArch understands.

It also inserts "\n" as a not a line breaker in the file. To get rid of it: `sed 's/\\n//g' < chew.json > chew.noLineBreaks.json`

#### Weblogo

`weblogo -n 500 -c chemistry -A protein -D fasta -Y NO --errorbars NO -f $alignment -o $output.eps`

#### RAxML

`raxmlHPC-PTHREADS-AVX -T 40 -m PROTGAMMAIAUTO -p 1234555 -x 9876545 -f a -N 1000 -s $alignment.fa -q $partition.par -n $output.nwk`

#### TreeColapse

`java -jar ~/Downloads/TreeCollapseCL4.jar -b 50 -f $tree`
